import { Plate } from './../models/plate.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-plate',
  templateUrl: './plate.component.html',
  styleUrls: ['./plate.component.scss']
})
export class PlateComponent implements OnInit {
  @Input() set plate(value) {
    this.selectedPlate = value;
    this.createPlateArray();
    this.selectedColumns = [];
    this.selectedColumnsChange.emit(this.selectedColumns);
    this.fillColumnNumbers();
  }

  /** Selected plate */
  public selectedPlate: Plate = {
    wells: 96,
    cols: 12,
    rows: 8
  };

  /** Plate array */
  public plateArray = [];
  /** Alphabet for plate columns */
  private alphabet = 'abcdefghijklmnopqrstuvwxyz';
  /** Columns array */
  public columns = [];

  /** Selected column index */
  public selectedColumnIndex: number;

  /** selected columns index array */
  @Input() selectedColumns = [];
  @Output() selectedColumnsChange: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    this.createPlateArray();
    this.fillColumnNumbers();
  }

  /** Fill columns with number */
  private fillColumnNumbers(): void {
    this.columns = [];
    this.columns.push('');
    for (let i = 0; i < this.selectedPlate.cols; i++) {
      this.columns.push(this.alphabet[i]);
    }
  }

  /** Create plate array using selected plate data */
  private createPlateArray(): void {
    this.plateArray = [];
    for (let i = 0; i < this.selectedPlate.rows; i++) {
      this.plateArray[i] = [];
      for (let j = 0; j < this.selectedPlate.cols; j++) {
        this.plateArray[i][j] = false;
      }
    }
  }

  /**
   * Select column by index
   * @param index column index
   */
  public selectColumn(index: number): void {
    if (index > 0) {
      if (!this.selectedColumns[index - 1]) {
        this.selectedColumns[index - 1] = index;
      } else {
        delete this.selectedColumns[index - 1];
      }
    }
    this.selectedColumnsChange.emit(this.selectedColumns);
  }

  /** On cell mouseover */
  public enterCell(index: number): void {
    if (index > 0) {
      this.selectedColumnIndex = index;
    }
  }

  /** On cell mouseleave */
  public leaveCell(index: number): void {
    if (index > 0) {
      this.selectedColumnIndex = null;
    }
  }

  /** Return boolean if column is selected */
  public checkIndex(index: number): boolean {
    return !!this.selectedColumns[index - 1];
  }
}
