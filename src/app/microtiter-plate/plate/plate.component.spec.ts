import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlateComponent } from './plate.component';
import { By } from '@angular/platform-browser';

describe('PlateComponent', () => {
  let component: PlateComponent;
  let fixture: ComponentFixture<PlateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlateComponent);
    component = fixture.componentInstance;
    fixture.debugElement.query(By.css('.plate-container')).nativeElement.style = 'z-index: -1';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default plate with 96 cells', () => {

    expect(component.selectedPlate.wells).toEqual(96);
  });

  it('should fill columns array with chars', () => {
    // columns array should have one more element because first is empty
    expect(component.columns.length).toEqual(component.selectedPlate.cols + 1);

    expect(component.columns[1]).toBe('a');
  });

  it('should create plate array', () => {
    expect(component.plateArray.length).toEqual(component.selectedPlate.rows);

    expect(component.plateArray[0].length).toEqual(
      component.selectedPlate.cols
    );
  });

  it('should have number of html cells equal to selected plate number of wells', () => {
    const cells = fixture.debugElement.queryAll(By.css('.plate-cell'));
    fixture.detectChanges();

    expect(cells.length).toBe(component.selectedPlate.wells);
  });

  it('should select and deselect columns on cell click', () => {
    const cells = fixture.debugElement.queryAll(By.css('.plate-cell'));
    const firstCell = cells[0].nativeElement;
    const secondCell = cells[1].nativeElement;
    fixture.detectChanges();

    firstCell.click();
    fixture.detectChanges();

    expect(component.selectedColumns.length).toBe(1);
    expect(component.selectedColumns[0]).toBe(1);

    secondCell.click();
    fixture.detectChanges();

    expect(component.selectedColumns.length).toBe(2);
    expect(component.selectedColumns[1]).toBe(2);

    firstCell.click();
    fixture.detectChanges();

    expect(component.selectedColumns[0]).toBe(undefined);
  });

  it('should select and deselect columns on column character click', () => {
    const cells = fixture.debugElement.queryAll(By.css('.plate-column'));
    const firstCol = cells[1].nativeElement;
    const secondCol = cells[2].nativeElement;
    component.selectedColumns = [];
    fixture.detectChanges();

    firstCol.click();
    fixture.detectChanges();

    expect(component.selectedColumns.length).toBe(1);
    expect(component.selectedColumns[0]).toBe(1);

    secondCol.click();
    fixture.detectChanges();

    expect(component.selectedColumns.length).toBe(2);
    expect(component.selectedColumns[1]).toBe(2);

    firstCol.click();
    fixture.detectChanges();

    expect(component.selectedColumns[0]).toBe(undefined);
  });

  it('should highlight column on cell mouseover', () => {
    const cells = fixture.debugElement.queryAll(By.css('.plate-cell'));
    const firstCell = cells[0].nativeElement;
    const secondCell = cells[1].nativeElement;
    fixture.detectChanges();

    firstCell.dispatchEvent(new Event('mouseover'));
    fixture.detectChanges();

    expect(component.selectedColumnIndex).toBe(1);

    firstCell.dispatchEvent(new Event('mouseleave'));
    fixture.detectChanges();

    expect(component.selectedColumnIndex).toBe(null);
  });
});
