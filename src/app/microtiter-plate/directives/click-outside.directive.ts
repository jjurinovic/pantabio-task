import { Directive, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appClickOutside]'
})
export class ClickOutsideDirective {

  @Output() clickOutside: EventEmitter<boolean> = new EventEmitter();

  @HostListener('document:click', ['$event'])
  public onclick(event) {
    if (!this.el.nativeElement.contains(event.target)) {
      this.clickOutside.emit(true);
    }
  }

  constructor(private el: ElementRef) { }

}
