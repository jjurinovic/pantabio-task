import { ClickOutsideDirective } from './directives/click-outside.directive';
import { PlateComponent } from './plate/plate.component';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { MicrotiterPlateComponent } from './microtiter-plate.component';
import { By } from '@angular/platform-browser';

describe('IntegrationMicrotiterComponent', () => {
  let component: MicrotiterPlateComponent;
  let fixture: ComponentFixture<MicrotiterPlateComponent>;
  let childComp: PlateComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MicrotiterPlateComponent,
        PlateComponent,
        ClickOutsideDirective
      ],
      imports: [FormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicrotiterPlateComponent);
    component = fixture.componentInstance;
    childComp = fixture.debugElement.query(By.directive(PlateComponent)).componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select selectedPlate', () => {
    const buttons = fixture.debugElement.queryAll(By.css('button'));
    const btn = buttons[0].nativeElement;

    btn.click();
    fixture.detectChanges();
    expect(childComp).toBeTruthy();
    expect(childComp.selectedPlate.wells).toEqual(6);
  });

  it('should change selected columns array in plate component', () => {

    component.columnArray = [1, 2, 3];
    fixture.detectChanges();
    expect(childComp.selectedColumns).toEqual([1, 2, 3]);
  });


  it('should select columns in plate component and highlight them', () => {
    component.inputValue = '1-3';
    component.checkValue({});
    fixture.detectChanges();
    expect(childComp.selectedColumns).toEqual([1, 2, 3]);

    const el = fixture.debugElement.query(By.directive(PlateComponent));
    const cells = el.queryAll(By.css('.plate-cell'));

    // third cell should be highlighted
    expect(cells[2].nativeElement.classList.contains('plate-cell-selected')).toBeTruthy();
  });

  it('should fill input with selected column numbers from plate component', () => {
    fixture.detectChanges();

    const el = fixture.debugElement.query(By.directive(PlateComponent));
    const cells = el.queryAll(By.css('.plate-cell'));
    cells[0].nativeElement.click();
    cells[1].nativeElement.click();
    cells[2].nativeElement.click();
    cells[4].nativeElement.click();
    fixture.detectChanges();
    expect(component.inputValue).toBe('1-3,5');

    //  click to disable some columns
    cells[1].nativeElement.click();
    cells[4].nativeElement.click();
    fixture.detectChanges();

    expect(component.inputValue).toBe('1,3');
  });
});
