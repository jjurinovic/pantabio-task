import { FormsModule } from '@angular/forms';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { PlateComponent } from './plate/plate.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MicrotiterPlateComponent } from './microtiter-plate.component';

@NgModule({
  declarations: [
    MicrotiterPlateComponent,
    PlateComponent,
    ClickOutsideDirective
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    MicrotiterPlateComponent,
    PlateComponent,
    ClickOutsideDirective
  ]
})
export class MicrotiterPlateModule { }
