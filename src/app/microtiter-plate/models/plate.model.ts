export interface Plate {
  wells: number;
  cols: number;
  rows: number;
}
