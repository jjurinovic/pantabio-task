import { Plate } from './models/plate.model';

export const plates: Plate[] = [
  {
    wells: 6,
    cols: 3,
    rows: 2
  },
  {
    wells: 12,
    cols: 4,
    rows: 3
  },
  {
    wells: 24,
    cols: 6,
    rows: 4
  },
  {
    wells: 48,
    cols: 8,
    rows: 6
  },
  {
    wells: 96,
    cols: 12,
    rows: 8
  },
  {
    wells: 384,
    cols: 24,
    rows: 16
  }
];
