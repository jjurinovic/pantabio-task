import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicrotiterPlateComponent } from './microtiter-plate.component';
import { By } from '@angular/platform-browser';

describe('MicrotiterPlateComponent', () => {
  let component: MicrotiterPlateComponent;
  let fixture: ComponentFixture<MicrotiterPlateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicrotiterPlateComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicrotiterPlateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill column array with numbers from input separated by comma', async () => {
    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('.column-choose-input'));
      const el = input.nativeElement;

      const event = new KeyboardEvent('keyup');

      fixture.componentInstance.inputValue = '1';
      el.value = '1,2,3';
      el.dispatchEvent(new Event('input'));
      el.dispatchEvent(event);

      fixture.detectChanges();

      expect(fixture.componentInstance.columnArray).toEqual([1, 2, 3]);
    });
  });

  it('should change input value with value from component ', async () => {
    const input = fixture.debugElement.query(By.css('.column-choose-input'));
    const el = input.nativeElement;
    fixture.componentInstance.inputValue = '1,2,3';
    fixture.detectChanges();

    fixture.whenStable().then(() => {

      expect(el.value).toEqual('1,2,3');
    });
  });

  it('should fill column array with numbers and numbers from range', async () => {
    const input = fixture.debugElement.query(By.css('.column-choose-input'));
    const el = input.nativeElement;
    const event = new KeyboardEvent('keyup');
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      el.value = '1-3,4-8,9,10';
      el.dispatchEvent(new Event('input'));
      el.dispatchEvent(event);

      fixture.detectChanges();

      expect(fixture.componentInstance.columnArray).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    });
  });

  it('should sort input value on blur', async () => {

    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('.column-choose-input'));
      const el = input.nativeElement;

      const event = new KeyboardEvent('keyup');

      fixture.componentInstance.inputValue = '1';

      el.value = '5,6-9,1,12,2-3';
      el.dispatchEvent(new Event('input'));
      el.dispatchEvent(event);

      fixture.detectChanges();

      el.dispatchEvent(new Event('blur'));
      fixture.detectChanges();

      expect(fixture.componentInstance.inputValue).toEqual('1,2-3,5,6-9,12');
    });
  });


  it('should have default selected plate with 96 wells', async () => {
    expect(component.selectedPlate.wells).toBe(96);
  });

  it('should open plate window', () => {
    const button = fixture.debugElement.query(By.css('.plate-button'));
    const btn = button.nativeElement;

    btn.click();
    fixture.detectChanges();
    expect(component.plateVisible).toBeTruthy();
  });
});
