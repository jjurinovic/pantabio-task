import { plates } from './plates';
import { Plate } from './models/plate.model';
import { Component, OnInit, ChangeDetectorRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-microtiter-plate',
  templateUrl: './microtiter-plate.component.html',
  styleUrls: ['./microtiter-plate.component.scss']
})
export class MicrotiterPlateComponent implements OnInit {
  /** Value from input */
  public inputValue: string;

  /** Total number of columns */
  public numberOfColumns = 12;
  /** Array for columns */
  public columnArray = [];

  /** Too big number error */
  public error = false;
  /** Error text */
  public errorMessages = [];

  public plateVisible = false;

  /** Selected plate */
  public selectedPlate: Plate;
  /** All plates */
  public plates: Plate[] = plates;

  get columns() {
    return this.columnArray;
  }

  set columns(value) {
    this.columnArray = [];
    this.inputValue = '';
    this.cdr.detectChanges();
    value.forEach(item => {
      this.columnArray[item - 1] = item;
    });

    let counter = 0;
    let arr = [];
    let val = '';

    // loop throught all numbers in array and create ranges if possible
    for (let i = 0; i < this.numberOfColumns; i++) {
      if (!!this.columnArray[i]) {
        arr.push(this.columnArray[i]);
        counter++;
      } else {
        counter = 0;
        if (arr.length > 1) {
          val += `${arr[0]}-${arr[arr.length - 1]},`;
        } else if (arr.length === 1) {
          val += `${arr[0]},`;
        }
        arr = [];
      }
    }

    // if last column is in array, it should be added to value
    if (arr.length > 1) {
      val += `${arr[0]}-${arr[arr.length - 1]}`;
    } else if (arr.length === 1) {
      val += `${arr[0]}`;
    }
    this.inputValue = val;

    // if last character is comma, remove it
    if (this.inputValue[this.inputValue.length - 1] === ',') {
      this.inputValue = this.inputValue.substring(
        0,
        this.inputValue.length - 1
      );
    }
  }

  constructor(
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.selectPlate(this.plates[4]);
  }

  /** Restrict keyboard inputs only to numbers, comma and '-' character */
  public restrictNumeric(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    if (e.which === 45 || e.which === 44) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  /** Validate input value and create array */
  public checkValue(e) {
    this.errorMessages = []; // clean error messages

    const arr = this.inputValue.split(',');
    // if last element isn't number
    if (arr[arr.length - 1] === '') {
      arr.pop();
    }

    if (this.inputValue && e.key !== ',') {
      let error = false;
      const columns = [];

      arr.forEach(item => {
        // check if string is range
        if (
          item.includes('-') &&
          !item.startsWith('-') &&
          item[item.length - 1] !== '-'
        ) {
          // create array from range
          const items = this.convertRange(item);

          // combine range array with column array
          items.forEach(el => {
            if (el && !columns[el - 1]) {
              columns[el - 1] = el;
            } else if (!!columns[el - 1]) {
              error = true;
              this.showError(`Column number: ${el} is already used!`);
            }
          });
        } else {
          // error check
          error = this.checkErrors(item, columns);
        }
      });

      // if there is no errors, hide error box and assign columns to main array
      if (!error) {
        this.error = false;
        this.columnArray = columns;
      }
    } else if (this.inputValue.includes(',,')) {
      this.showError('Column number must start with number');
    } else {
      this.error = false;
    }

    /** Clean array if there is no numbers in string */
    if (this.inputValue === '') {
      this.columnArray = [];
    }
  }

  /**
   * Return array from range string
   * @param range range string, eg. 1-3
   */
  public convertRange(range: string) {
    const rangeArray = range.split('-');
    const firstNumber = parseInt(rangeArray[0], 10);
    const lastNumber = parseInt(rangeArray[1], 10);

    const columns = [];

    if (rangeArray.length === 2 && !!firstNumber && !!lastNumber) {
      if (firstNumber > this.numberOfColumns) {
        this.showError(`${firstNumber} in range (${range}) is too big!`);
      } else if (lastNumber > this.numberOfColumns) {
        this.showError(`${lastNumber} in range (${range}) is too big!`);
      } else if (firstNumber === 0 || lastNumber === 0) {
        this.showError('0 is not a column number!');
      } else if (firstNumber > lastNumber) {
        this.showError('First number must be greater than second!');
      } else {
        for (let i = firstNumber; i < lastNumber + 1; i++) {
          if (!columns[i - 1]) {
            columns[i - 1] = i;
          }
        }
        return columns;
      }
    }
  }

  /**
   * Check errors and return boolean. Also set values if no errors
   * @param item string value
   */
  private checkErrors(item: string, columns): boolean {
    if (item.startsWith('0')) {
      this.showError('0 is not a column number!');
      return true;
    } else if (item[0] === '-' || item[0] === ',') {
      this.showError(`Column number can't start with '-'!`);
      return true;
    } else if (item[item.length - 1] === '-') {
      this.showError(`Column range must have second number!`);
      return true;
    } else {
      const num = parseInt(item, 10);
      if (num > this.numberOfColumns) {
        this.showError(`Column with number: ${num} doesn't exist!`);
        return true;
      } else if (!!columns[num - 1]) {
        this.showError(`Column number: ${num} is already used!`);
        return true;
      } else {
        columns[num - 1] = num;
        return false;
      }
    }
  }

  /**
   * Show error dialog with message for input
   * @param text error text
   */
  private showError(text: string): void {
    const index = this.errorMessages.indexOf(text);

    // if error message doesn't exist
    if (index < 0) {
      this.errorMessages.push(text);
      this.error = true;
    }
  }

  /** Sort input value */
  public sortInputValue(): void {
    if (!this.error) {
      let arr = this.inputValue.split(',');

      arr = arr.sort((a, b) => {
        const x = parseInt(a, 10);
        const y = parseInt(b, 10);

        if (x < y) {
          return -1;
        } else if (y < x) {
          return 1;
        }
        return 0;
      });

      this.inputValue = arr.toString();
    }
  }

  /** Toggle visibility of plate component */
  public togglePlate(): void {
    this.plateVisible = !this.plateVisible;
  }

  /** Selecte plate */
  public selectPlate(plate: Plate): void {
    this.selectedPlate = plate;
    this.numberOfColumns = plate.cols;
  }
}
